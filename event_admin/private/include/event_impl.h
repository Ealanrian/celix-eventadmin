//
// Created by ealanrian on 1-6-15.
//

#ifndef CELIX_EVENT_IMPL_H
#define CELIX_EVENT_IMPL_H
celix_status_t eventAdmin_createEvent(event_admin_pt event_admin, char *topic, properties_pt properties, event_pt *event);

celix_status_t eventAdmin_containsProperty(event_pt *event, char *property, bool *result);

celix_status_t eventAdmin_event_equals(event_pt *event, event_pt *compare, bool *result);

celix_status_t eventAdmin_getProperty(event_pt *event, char *propertyKey, char **propertyValue);

celix_status_t eventAdmin_getPropertyNames(event_pt *event, array_list_pt *names);

celix_status_t eventAdmin_getTopic(event_pt *event, char **topic);

celix_status_t eventAdmin_hashCode(event_pt *event, int *hashCode);

celix_status_t eventAdmin_matches(event_pt *event);

celix_status_t eventAdmin_toString(event_pt *event, char *eventString);
#endif //CELIX_EVENT_IMPL_H
